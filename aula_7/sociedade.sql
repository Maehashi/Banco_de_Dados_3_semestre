create database Populacao;

use Populacao;

create table pessoa
(
	id int primary key not null,
	idade int,
    nIrmaos int,
	praticaEsportes int,
    altura double,
    distancia double
);

INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (1, 19, 1, 6, 1.73, 15);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (2, 18, 1, 6, 1.83, 13);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (3, 19, 1, 6, 1.72, 47);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (4, 23, 1, 1, 1.73, 15);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (5, 21, 3, 4, 1.8, 15);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (6, 19, 0, 0, 1.8, 8);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (7, 18, 0, 0, 1.78, 53);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (8, 29, 3, 0, 1.9, 9);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (9, 19, 1, 0, 1.82, 31);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (10, 32, 0, 0, 1.76, 2);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (11, 22, 1, 0, 1.9, 2);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (12, 19, 1, 0, 1.77, 47);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (13, 18, 1, 0, 1.75, 47);
INSERT INTO pessoa (id, idade, nIrmaos, praticaEsportes, altura, distancia)VALUES (14, 19, 1, 1, 1.77, 45);
