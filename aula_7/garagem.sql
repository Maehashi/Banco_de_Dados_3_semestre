CREATE DATABASE Garagem;

USE Garagem;

CREATE TABLE cliente
(
	codigo INT NOT NULL,
    nome VARCHAR (150),
    rg VARCHAR (9) UNIQUE,
    cpf VARCHAR (10) UNIQUE,
    telefone VARCHAR (14),
    endereco VARCHAR (200),
    cidade VARCHAR (100),
    estado VARCHAR (100)
);

CREATE TABLE carros
(
	codigo INT NOT NULL,
    marca VARCHAR (100),
    modelo VARCHAR (100),
    ano INT,
    cor VARCHAR (100),
    placa VARCHAR(8) UNIQUE,
    FOREIGN KEY (cod_usuario) REFERENCES cliente (codigo)
);