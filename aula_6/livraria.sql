# criar database livraria

create database Livraria;

# Selecionar Banco de dados
use Livraria;

# criar tabela livro (nome, preco)

create table livro 
(
	id int primary key not null,
	nome varchar(100),
    preco double
);

# Inserir registros de livro (Java, 95.00, CSS, 55.00)

INSERT INTO livro (id, nome, preco)VALUES (1, "Java", "110.00");
INSERT INTO livro (id, nome, preco)VALUES (2, "CSS", "55.00");
INSERT INTO livro (id, nome, preco)VALUES (3, "cronicas de narnia", "5.00");
INSERT INTO livro (id, nome, preco)VALUES (4, "as 21 leis da liderança","12.00");
INSERT INTO livro (id, nome, preco)VALUES (5, "senhor doe aneis", "45.00");


# Renomear  tabela para livros 

ALTER table livro RENAME livros;

# Atualizar preços de livro (Java, 110.00; CSS, 80.00)

update livros set preco_livros = 110.00 WHERE nome_livro = "Java" ;

# Selecionar o livro Java 

select * from livros;

# ver tabela 

show tables;

#Adicionar uma linha a tabela 

alter table livros ADD codigo int primary key auto_increment;

# Selecionar apenas o livro java

select * from livros where nome_livro= "Java";

#Deletar o livro "Java"

delete from livros where id_livro=1;

#criar autores 

create table Autores
(
	id int key not null,
    nome varchar (100)
);

#Incerir autores 

INSERT INTO Autores (id, nome)VALUES (1, "joao");
INSERT INTO Autores (id, nome)VALUES (2, "marcio");
INSERT INTO Autores (id, nome)VALUES (3, "fernando");
INSERT INTO Autores (id, nome)VALUES (4, "gustavo");


#Deletar tabela 

drop table Livro;
drop table Autores;



