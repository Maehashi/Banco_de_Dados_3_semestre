# Script Aula BD 06.04.2017

create database campeonato;

# verdatabeses
show databases;

# Selecionar Banco de dados
use campeonato;

# cria nova tabela
CREATE TABLE Equipe  
(  
    cod_equipe int primary key not null,
    nome_equipe varchar (50),
    cor_pred_equipe varchar (10)
);

CREATE TABLE Jogador 
(
	cod_jogador int primary key not null,
	nome_jogador varchar (50),
	posicao varchar (20),
	idade int
);

# ver Tabelas
show tables;

INSERT INTO Equipe (cod_equipe, nome_equipe, cor_pred_equipe)
VALUES (1, "São Paulo", "Rosa");

INSERT INTO Equipe (cod_equipe, nome_equipe, cor_pred_equipe)
VALUES (2, "Corintia", "Brancos");

INSERT INTO Equipe (cod_equipe, nome_equipe, cor_pred_equipe)
VALUES (3, "Palmeiras", "Verde");

INSERT INTO Equipe (cod_equipe, nome_equipe, cor_pred_equipe)
VALUES (4, "Mac", "Azul");

# deletar tabela
drop table Equipe;

 Select * from equipe;