# Banco de  dados

Caraquiteristica|Resposta                  
---------------:|:--------
 Profassor:     | Ricado Favam             
 Asunto:        | Banco de dados           
 Data:          | Fevereiro à Junho de 2018

## Primeira aula

Na primeira foi instalado: o [MySql Workbench](https://dev.mysql.com/downloads/workbench/), [Sql Sever](https://docs.microsoft.com/pt-br/sql/linux/quickstart-install-connect-ubuntu)

# Comandos do MySql Workbench

## Criar Banco de dados

```sql
CREATE DATABASE Nome_do_banco_de_dados;
```

## Comando para defini o Banco que vai ser usado

```sql
USE Nome_do_banco_de_dados;
``` 

## Criar nova Tabela

```sql
CREATE TABLE NOME_DA_TABELA
(
    codigo int NOT NULL auto_increment,
    nome VARCHAR(100) NOT NULL,
    rg VARCHAR(9) NOT NULL unique,
    cpf VARCHAR(11) NOT NULL unique,
    endereco VARCHAR(100),
    cidade VARCHAR(50),
    estado VARCHAR(2),
    PRIMARY KEY(codigo)
);
```

# Linhas da tabela

## Tipos

Ty p e | S i z e | D e s c r i p t i o n
------:|:-------:|:---------------------
CHAR[Length]|Length bytes|A fixed-length field from 0 to 255 characters long.
VARCHAR(Length) | String length + 1 bytes | A fixed-length field from 0 to 255 characters long.
TINYTEXT|String length + 1 bytes|A string with a maximum length of 255 characters.
TEXT|String length + 2 bytes|A string with a maximum length of 65,535 characters.
|MEDIUMTEXT|String length + 3 bytes|A string with a maximum length of 16,777,215 characters.
LONGTEXT|String length + 4 bytes|A string with a maximum length of 4,294,967,295 characters.
TINYINT[Length]|1 byte|Range of -128 to 127 or 0 to 255 unsigned.
SMALLINT[Length]|2 bytes|Range of -32,768 to 32,767 or 0 to 65535 unsigned.
MEDIUMINT[Length]|3 bytes|Range of -8,388,608 to 8,388,607 or 0 to 16,777,215|unsigned.
INT[Length]|4 bytes|Range of -2,147,483,648 to 2,147,483,647 or 0 to 4,294,967|295 unsigned.
BIGINT[Length]|8 bytes|Range of -9,223,372,036,854,775,808 to 9,223,372,036,854|775,807 or 0 to 18,446,744,073,709,551,615 unsigned.
FLOAT|4 bytes|A small number with a floating decimal point.
DOUBLE[Length, Decimals]|8 bytes|A large number with a floating decimal point.
DECIMAL[Length, Decimals]|Length + 1 or Length + 2 bytes|A DOUBLE stored as a string, allowing for a fixed decimal|point.
DATE|3 bytes|In the format of YYYY-MM-DD.|
ATETIME|8 bytes|In the format of YYYY-MM-DD HH:MM:SS.
TIMESTAMP|4 bytes|In the format of YYYYMMDDHHMMSS; acceptable range ends|inthe year 2037.
TIME|3 bytes|In the format of HH:MM:SS|ENUM|1 or 2 bytes|Short for enumeration, which means that each column can|haveone of several possible values.
SET|1, 2, 3, 4, or 8 bytes|Like ENUM except that each column can have more than one|ofseveral possible values.

## Primary Key (Chave Primaria)

A chave primária, ou PRIMARY KEY, é o conceito mais básico relacionado à organização em um banco de dados. Toda tabela irá possuir uma, e somente uma, chave primária. Essa chave é utilizada como o identificador único da tabela, sendo, então, representada, por aquele campo (ou campos) que não receberá valores repetidos.

```sql
CREATE TABLE NOME_DA_TABELA(
    codigo int NOT NULL auto_increment,
    PRIMARY KEY(codigo)
);
```

## FOREIGN KEY



```sql
foreign key (NOME_QUE_VAI_FICAR_NA_TABELA) references NOME_DA_TABELA(NOME_DA_PRIMARY_KEY)
```


## AUTO_INCREMENT

O incremento automático permite que um número exclusivo seja gerado automaticamente quando um novo registro é inserido em uma tabela.
    Muitas vezes, esse é o campo-chave primário que gostaríamos de criar automaticamente toda vez que um novo registro fosse inserido.

```sql
CREATE TABLE NOME_DA_TABELA(
    codigo int NOT NULL auto_increment,
    PRIMARY KEY(codigo)
);
```

## NOT NULL
 NOT NULL ou "não é campo sem valor" ele obrivaga que a linha seja preenchida.

```sql
CREATE TABLE NOME_DA_TABELA
(
    NOME_COLUNA_1 VARCHAR(10) NOT NULL,
    NOME_COLUNA_2 VARCHAR(10) NOT NULL,
    NOME_COLUNA_3 VARCHAR(10) NOT NULL
);    
```

##UNIQUE

A restrição UNIQUE garante que todos os valores em uma coluna sejam diferentes.

As restrições UNIQUE e PRIMARY KEY fornecem uma garantia de exclusividade para uma coluna ou conjunto de colunas.

Uma restrição PRIMARY KEY tem automaticamente uma restrição UNIQUE.

No entanto, você pode ter muitas restrições UNIQUE por tabela, mas apenas uma restrição PRIMARY KEY por tabela.

```sql
CREATE TABLE NOME_DA_TABELA
(
    NOME_COLUNA_1 VARCHAR(10) NOT NULL unique,
    NOME_COLUNA_2 VARCHAR(10) NOT NULL unique
);
```

## INSERT INTO

A instrução INSERT INTO é usada para inserir novos registros em uma tabela.

```sql
Insert into NOME_DA_TABELA (codigo, nome, rg, cpf, endereco, cidade, estado)
values(1, "Clodoaldo Samsão","254587859","87536587469", "R. das Acácias, 25", "Pindamonhangaba", "SP");
```

## SELECT   

O comando SELECT permite recuperar os dados de um objeto do banco de dados, como uma tabela, view e, em alguns casos, uma stored procedure (alguns bancos de dados permitem a criação de procedimentos que retornam valor). A sintaxe mais básica do comando é:

```sql
SELECT <lista_de_campos> 
FROM <nome_da_tabela></nome_da_tabela></lista_de_campos>
```
### Para selecionar uma tabela

```sql
SELECT * FROM NOME_DA_TABELA
```

## Comentario no MySql

```sql
# comentario

/*
Comentario
Comentario
*/

```

