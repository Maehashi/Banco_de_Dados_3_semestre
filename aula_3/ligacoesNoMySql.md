# Relação entre tabelas em banco de dados relacional 

## banco de dado relacional

Um banco de dados relacional é uma coleção de dados com relacionamentos predefinidos entre si. Esses itens são organizados como um conjunto de tabelas com colunas e linhas. As tabelas são usadas para reter informações sobre os objetos a serem representados no banco de dados. Cada coluna da tabela retém um determinado tipo de dado e um campo armazena o valor em si de um atributo. As linhas na tabela representam uma coleção de valores relacionados de um objeto ou uma entidade. Cada linha em uma tabela pode ser marcada com um único identificador chamado de chave principal. Já as linhas entre as várias tabelas podem ser associadas usando chaves estrangeiras. Esses dados podem ser acessados de várias formas diferentes, sem reorganizar as tabelas do banco de dados eles mesmos.

Fonte: https://aws.amazon.com/pt/relational-database/


## O que são relações tabela

Em um banco de dados relacional, as relações permitem que você evite dados redundantes. Por exemplo, se você estiver desenvolvendo um banco de dados que irá rastrear informações sobre livros, será necessário ter uma tabela chamada Títulos, que irá armazenar as informações sobre cada livro, tais como o título do livro, a data de publicação e o editor. Também há informações sobre o editor que talvez você deseje armazenar, tais como o número de telefone, o endereço e o código postal do editor. Se você quisesse armazenar todas essas informações na tabela de títulos, o número de telefone do editor seria duplicado para cada título que ele publicasse.

A melhor solução é armazenar as informações do editor somente uma vez em uma tabela separada, Editores. Assim, você poderá colocar um ponteiro na tabela Títulos para referir a uma entrada na tabela Editores.

Para certificar-se de que seus dados não estão fora de sincronismo, é possível reforçar a integridade referencial entre as tabelas Títulos e Editores. As relações de integridade referencial ajudam a garantir que as informações em uma tabela correspondem às informações em outra tabela. Por exemplo, cada título na tabela Títulos deve estar associado a um editor específico na tabela Editores. Um título não pode ser adicionado ao banco de dados para um editor que não existe no banco de dados.

## Tipos de relações tabela

Uma relação trabalha correspondendo dados nas colunas chave, geralmente as colunas com o mesmo nome em ambas as tabelas. Na maioria dos casos, as relações correspondem a chave primária de uma tabela, que fornece um identificador exclusivo para cada linha, à entrada na chave estrangeira da outra tabela. Por exemplo, as vendas podem ser associadas aos títulos específicos vendidos criando uma relação entre a coluna título_id na tabela Títulos (a chave primária) e a coluna título_id na tabela Vendas (a chave estrangeira).

Existem três tipos de relações entre as tabelas: O tipo de relação criado depende de como as colunas relacionadas são definidas.

## Relações um-para-muitos

Uma relação um-para-muitos é o tipo mais comum de relação. Neste tipo de relação, uma linha na tabela A pode ter muitas linhas correspondentes na tabela B, mas uma linha na tabela B pode apenas ter uma linha correspondente na tabela A. Por exemplo, as tabelas Editores e Títulos têm uma relação um-para-muitos: cada editor produz muitos títulos, mas cada título é produzido por apenas um editor.

Um relação um-para-muitos será criada se uma das colunas 
relacionadas for uma chave primária ou tiver restrição Unique.

No Access, o lado da chave primária de uma relação um-para-muitos é marcado por símbolo de chave. O lado da chave estrangeira de uma relação é marcado por um símbolo infinito.

## Relações muitos-para-muitos

Em uma relação muitos-para-muitos, uma linha na tabela A pode ter muitas linhas correspondentes na tabela B e vice-versa. Você cria essa relação definindo uma terceira tabela, chamada tabela de junção, cuja chave primária consiste das chaves estrangeiras de ambas as tabelas A e B. Por exemplo, a tabela Autores e a tabela Títulos têm uma relação muitos-para-muitos definida por uma relação um-para-muitos de cada uma dessas tabelas na tabela TítuloAutores. A chave primária da tabela TítuloAutores é a combinação da coluna au_id (a chave primária da tabela Autores) e da coluna título_id (a chave primária da tabela Títulos).

## Relações um-para-um

Em uma relação um-para-um, uma linha na tabela A pode ter mais de uma linha correspondente na tabela B e vice-versa. Um relação um-para-um será criada se ambas as colunas relacionadas forem chaves principais ou tiverem restrições Unique.

Este tipo de relação não é comum, pois a maioria das informações relacionadas dessa forma estão em uma tabela. Você deve usar uma relação um-para-um para: 

* Dividir uma tabela em várias colunas.
* Isolar parte de uma tabela por motivos de segurança.
* Armazenar dados de curta duração e que podem ser facilmente excluídos apenas excluindo a tabela.
* Armazenar informações que se aplicam somente a um subconjunto da tabela principal.

No Access, o lado da chave primária de uma relação um-para-um é marcado por símbolo de chave. O lado da chave estrangeira também é marcado por um símbolo de chave.


Fonte: https://support.microsoft.com/pt-br/help/304466/how-to-define-relationships-between-tables-in-an-access-database