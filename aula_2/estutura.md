# Desafio da Biblioteca

Uma biblioteca mantém um conjunto de livros de diversas categorias. Cada livro tem sempre uma categoria classificada de uma lista de categorias que contem código, descrição.
Os exemplares dos livros estão organizados e dispostos em **estantes** apropriadas (numero e descrição da estante).
Um **livro** (código, titulo, ISBN) tem vários exemplares (código do livro, numero do exemplar no livro, data aquisição) na biblioteca. O numero do exemplar é gerado seqüencialmente em cada livro (obra).
Um livro tem pelo menos um autor, podendo ter vários (nome, nacionalidade). A publicação dos exemplares de um mesmo livro pode ter diferentes editoras (CNPJ, razão social), mas um exemplar é publicado por uma editora.
Na biblioteca trabalham várias bibliotecárias. Cada bibliotecária (nome e matricula) é responsável por organizar periodicamente sempre o mesmo conjunto de estantes, mas nem todas bibliotecárias tem esta atribuição.
As bibliotecárias realizam empréstimos de exemplares para os usuários da biblioteca (CPF, nome, endereço completo, telefones{ddd, numero}). Quando emprestado um exemplar, são guardados a data do empréstimo, a data prevista de devolução, o código do exemplar, o código do livro, o cpf do usuário e a matricula da bibliotecária).
Quando o usuário devolve o exemplar emprestado, a bibliotecária dá baixa do empréstimo e guarda as informações da devolução do empréstimo (data de devolução, os dados do empréstimo devolvido e a matricula da bibliotecária. Algumas bibliotecárias são estagiárias. Uma bibliotecária estagiária está sempre sob a responsabilidade de uma bibliotecária efetiva. No caso da estagiaria, deve-se saber também a instituição de ensino da qual a estagiária vem para fazer estagio (sigla e nome da universidade, e data de ingresso)
